// Change the icons in the circle-container
let iconIndex = 0;
showIcons();

function showIcons() {
  let i;
  let icons = document.getElementsByClassName("RmyIcon");
  for (i = 0; i < icons.length; i++)
    {
      icons[i].style.display = "none";
    }
  iconIndex++;
  if (iconIndex > icons.length)
    {
      iconIndex = 1
    }
  icons[iconIndex-1].style.display = "block";
  setTimeout(showIcons, 1000); // Change icon every 1 seconds
}
// =============================================================================
function toggleDisplay() {
  let hide = document.getElementById("Rhide-close");
  let content = document.getElementById('Rcircle-content');
  let fad = document.getElementById('Rfad');
  if ((hide.style.opacity=== '0') && (content.style.display==='none') && (fad.style.transform==='scale(1)'))
    {
      hide.style.opacity = '1';
      content.style.display = 'inline-block';
      fad.style.transform = 'scale(0.2)';
    } else {
      hide.style.opacity = '0';
      content.style.display = 'none';
      fad.style.transform = 'scale(1)';
    }
}
